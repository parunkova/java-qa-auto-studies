package ru.technokratos.qa.hotorcold;

import java.util.Random;
import java.util.Scanner;

import static java.lang.Math.abs;

public class HotOrColdGame {
//    Написать консольную игру горячо-холодно. Компьютер загадывает число от 1 до 100. Человек за n-ное количество
//    попыток пытается угадать. Программа должна быть юзерфрендли. Если в программе нельзя выиграть, то это незачёт.

    public static void main(String[] args) {
        int randomNumber = randomNumber();
        int inputNumber = inputNumber();
        if (inputNumber != randomNumber) {
            hotOrCold(inputNumber, randomNumber, 10);
        }
    }

    public static int randomNumber() {
        Random random = new Random();
        return random.nextInt(100) + 1;
    }

    public static int inputNumber() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите целое число от 1 до 100");
        if (in.hasNextInt()) {
            return in.nextInt();
        } else {
            System.out.println("Некорректный ввод");
            return inputNumber();
        }
    }

    public static int difference(int inputNumber, int randomNumber) {
        return abs(randomNumber - inputNumber);
    }

    public static void hotOrCold(int inputNumber, int randomNumber, int attemptsCount) {
        int previousDiff = difference(inputNumber, randomNumber);
        if (previousDiff < 20) {
            System.out.println("Тепло");
        } else {
            System.out.println("Холодно");
        }
        while (attemptsCount > 0) {
            inputNumber = inputNumber();
            if (inputNumber == randomNumber) {
                System.out.println("УРА! Победа!");
                break;
            } else {
                int currentDiff = difference(inputNumber, randomNumber);
                if (currentDiff < previousDiff) {
                    System.out.println("Теплее");
                } else if (currentDiff == previousDiff) {
                    System.out.println("Так же");
                } else {
                    System.out.println("Холоднее");
                }
                previousDiff = currentDiff;
                attemptsCount -= 1;
                System.out.printf("Осталось попыток: %d\n", attemptsCount);
            }
        }
        System.out.printf("Было загадано число %d\n", randomNumber);
    }
}
