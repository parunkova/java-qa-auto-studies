package ru.technokratos.qa.calculator;

import java.util.Objects;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        double firstNum = enterDouble();
        String operation = enterOperation();
        double secondNum = enterDouble();
        operationResult(firstNum, operation, secondNum);
    }

    public static double enterDouble() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число");
        double result = 0;
        if (in.hasNextDouble()) {
             result = in.nextDouble();
        } else {
            System.out.println("Некорректный ввод");
            enterDouble();
        }
        return result;
    }

    public static String enterOperation() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите операцию (+, -, *, /)");
        return in.next();
    }

    public static boolean isCorrectOperation(String operation, double secondNum) {
        if (Objects.equals(operation, "/") && secondNum == 0.0) return false;
        else return Objects.equals(operation, "+") || Objects.equals(operation, "-") ||
                Objects.equals(operation, "*") || Objects.equals(operation, "/");
    }

    public static double calculationResult(double firstNum, String operation, double secondNum) {
        switch (operation) {
            case "+":
                return firstNum + secondNum;
            case "-":
                return firstNum - secondNum;
            case "*":
                return firstNum * secondNum;
            case "/":
                return firstNum / secondNum;
        }
        System.out.println("Некорректная операция");
        throw new UnsupportedOperationException("Некорректная операция");
    }

    public static String operationResult(double firstNum, String operation, double secondNum) {
        String result;
        if (!isCorrectOperation(operation, secondNum)) {
            result = "Ошибка";
        } else result = "Результат: " + calculationResult(firstNum, operation, secondNum);
        System.out.println(result);
        return result;
    }
}
