package ru.technokratos.qa.calculator;

import java.util.Scanner;

public class OldCalculator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите первое число");
        double firstNum;
        if (in.hasNextDouble()) {
            firstNum = in.nextDouble();
        } else {
            System.out.println("Некорректный ввод. Введите число");
            return;
        }

        System.out.println("Введите операцию (+, -, *, /)");
        String operation = in.next();

        System.out.println("Введите второе число");
        double secondNum;
        if (in.hasNextDouble()) {
            secondNum = in.nextDouble();
        } else {
            System.out.println("Некорректный ввод. Введите число");
            return;
        }

        switch (operation) {
            case "+":
                System.out.printf("Результат: %f", firstNum + secondNum);
                break;
            case "-":
                System.out.printf("Результат: %f", firstNum - secondNum);
                break;
            case "*":
                System.out.printf("Результат: %f", firstNum * secondNum);
                break;
            case "/":
                if (secondNum == 0.0) {
                    System.out.println("Делить на 0 нельзя");
                }
                else {
                    System.out.printf("Результат: %f", firstNum / secondNum);
                }
                break;
            default:
                System.out.println("Некорректная операция");
        }
    }
}
