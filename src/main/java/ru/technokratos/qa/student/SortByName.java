package ru.technokratos.qa.student;

import java.util.Comparator;
import java.util.List;

public class SortByName implements Sort {

    @Override
    public void sort(List<Student> students) {
        students.sort(Comparator.comparing(Student::getName));
    }
}
