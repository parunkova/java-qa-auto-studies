package ru.technokratos.qa.student;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();

        students.add(new Student("Vasya", 23));
        students.add(new Student("Vova", 22));
        students.add(new Student("Ivan", 24));
        students.add(new Student("Maria", 21));
        students.add(new Student("Alina", 20));
        students.add(new Student("Daria", 23));
        students.add(new Student("Andrey", 19));
        students.add(new Student("Elena", 18));
        students.add(new Student("Diana", 22));
        students.add(new Student("Gennadi", 18));

        SortByName sortByName = new SortByName();
        sortByName.sort(students);

        for (Student student : students) {
            System.out.printf("%s, %d\n", student.getName(), student.getAge());
        }

        System.out.println("\n----------------------------\n");

        SortByAge sortByAge = new SortByAge();
        sortByAge.sort(students);

        for (Student student : students) {
            System.out.printf("%d, %s\n", student.getAge(), student.getName());
        }
    }
}
