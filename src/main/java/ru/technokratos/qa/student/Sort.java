package ru.technokratos.qa.student;

import java.util.List;

public interface Sort {
     void sort(List<Student> students);
}
