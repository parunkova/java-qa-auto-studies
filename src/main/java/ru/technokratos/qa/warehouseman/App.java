package ru.technokratos.qa.warehouseman;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        JuniorWarehouseman juniorWarehouseman = new JuniorWarehouseman();
        System.out.println(Arrays.toString(juniorWarehouseman.numbers));
        juniorWarehouseman.sort(juniorWarehouseman.numbers);
        System.out.println(Arrays.toString(juniorWarehouseman.numbers) + "\n");

        SeniorWarehouseman seniorWarehouseman = new SeniorWarehouseman();
        System.out.println(Arrays.toString(seniorWarehouseman.numbers));
        seniorWarehouseman.sort(seniorWarehouseman.numbers);
        System.out.println(Arrays.toString(seniorWarehouseman.numbers));
    }
}
