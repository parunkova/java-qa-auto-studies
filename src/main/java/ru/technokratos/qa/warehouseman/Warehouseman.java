package ru.technokratos.qa.warehouseman;

import static java.lang.Math.random;

public abstract class Warehouseman {

    protected int[] numbers;

    public Warehouseman() {
        this.numbers = randomOrderNumbers(10);
    }

    // Массив из рандомных чисел от 1 до 10
    protected int[] randomOrderNumbers(int arrayLength) {
        int[] numbers = new int[arrayLength];

        for (int i = 0; i < numbers.length; i ++) {
            numbers[i] = (int) (random() * 10) + 1;
        }
        return numbers;
    }
}
