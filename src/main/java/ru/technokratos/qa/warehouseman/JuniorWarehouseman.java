package ru.technokratos.qa.warehouseman;

public class JuniorWarehouseman extends Warehouseman {

    // Сортировка пузырьком
    public void sort(int[] numbers) {
        long start = System.nanoTime();
        for (int j = 0; j < numbers.length; j++) {
            for (int i = 1; i < numbers.length; i++) {
                if (numbers[i - 1] > numbers[i]) {
                    int temp = numbers[i - 1];
                    numbers[i - 1] = numbers[i];
                    numbers[i] = temp;
                }
            }
        }
        long stop = System.nanoTime();
        long diff = stop - start;
        System.out.printf("Сортировка заняла %d наносекунд\n", diff);
    }
}
