package ru.technokratos.qa.technokrater.core;

public abstract class ApiBaseTest {
    protected final String adminToken = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIxOSIsInJvbGVzIjpbIlJPTEVfVVNFUiIsIlJPTEVfQURNSU4iL" +
            "CJST0xFX09OQk9BUkRJTkdfQURNSU4iLCJST0xFX09OQk9BUkRJTkdfUkVDUlVJVEVSIiwiUk9MRV9VU0VSX0NSRUFURSIsIlJPTEV" +
            "fRE9DVU1FTlRTX0FDQ0VTUyIsIlJPTEVfUFJPRklMRV9VUERBVEUiLCJST0xFX0NBUl9BQ0NFU1MiLCJST0xFX1NFQVJDSCIsIlJPT" +
            "EVfRklSRV9FTVBMT1lFRSIsIlJPTEVfSFIiLCJST0xFX0FUVEVTVEFUSU9OX0FETUlOIiwiUk9MRV9TVEFGRl9ERVBBUlRNRU5UIiw" +
            "iUk9MRV9WQUNBVElPTl9DT05UUk9MIiwiUk9MRV9BVFRFU1RBVElPTl9IUiIsIlJPTEVfQVRURVNUQVRJT05fTEVBRCIsIlJPTEVfQ" +
            "VRURVNUQVRJT05fQ1JFQVRFIl19.bFtTv3lTr3vIaG5xTNB1IYWfSDQyLkfmIFKqB_kBMo0";

    protected final String userToken = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6IjI0NyIsImVtYWlsIjoicGFydW5rb3ZhQHRlY2hub2tyYXRvcy5jb20" +
            "iLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.QkUcYGSQRvM-iOG4TytEXWUBkZWDEnj0AnHTrlQ54yw";

    protected final String baseUrl = "https://technokrater-api.cloud.technokratos.com/employee-service";
 }
