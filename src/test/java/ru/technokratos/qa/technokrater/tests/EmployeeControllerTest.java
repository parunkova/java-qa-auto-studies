package ru.technokratos.qa.technokrater.tests;

import io.restassured.response.Response;
import org.testng.annotations.Test;
import ru.technokratos.qa.technokrater.core.ApiBaseTest;

import static io.restassured.RestAssured.given;

//    1. Покрыть тестами запрос /employee-controller/getAllUsingGET

public class EmployeeControllerTest extends ApiBaseTest {

    @Test(groups = "positive", priority = 1)
    public void testGetAllEmployeesSuccess() {
        Response response = given()
                .baseUri(baseUrl)
                .basePath("/api/employees")
                .relaxedHTTPSValidation()
                .header("authorization", adminToken)
                .when()
                .get();
        response.then().statusCode(200);
        System.out.println("Status code: " + response.statusCode());
    }

    @Test(groups = "negative", priority = 2)
    public void testGetAllEmployeesUnauthorized() {
        Response response = given()
                .baseUri(baseUrl)
                .basePath("/api/employees")
                .relaxedHTTPSValidation()
                .when()
                .get();
        response.then().statusCode(401);
        System.out.println("Status code: " + response.statusCode());
    }
}
