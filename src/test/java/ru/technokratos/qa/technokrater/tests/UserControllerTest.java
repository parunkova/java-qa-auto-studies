package ru.technokratos.qa.technokrater.tests;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import ru.technokratos.qa.technokrater.core.ApiBaseTest;

import java.util.*;

import static io.restassured.RestAssured.given;

//    2. Покрыть тестами запрос /user-controller/createUserUsingPOST
//       Без candidateId
//    3. Покрыть тестами запрос /user-controller/setUserRolesUsingPATCH
//       id получить при создании юзера

public class UserControllerTest extends ApiBaseTest {

    private int userId;

    private HashMap<String, java.io.Serializable> userParametersRequestBody() {
        UUID uuid = UUID.randomUUID();

        HashMap<String, java.io.Serializable> requestBody = new HashMap<>();
        requestBody.put("departmentId", 2);
        requestBody.put("directorId", 219);
        requestBody.put("email", uuid + "@randomemail");
        requestBody.put("firstName", "Апи");
        requestBody.put("hrId", 205);
        requestBody.put("isTrainee", true);
        requestBody.put("lastName", "Автотест");
        requestBody.put("phoneNumber", "+79999999999");
        requestBody.put("positionId", 5);
        requestBody.put("startDate", "2021-02-11");
        requestBody.put("teamId", 8);
        requestBody.put("toShowPatronymic", true);
        requestBody.put("workStatus", "BUSINESS_TRIP");

        return requestBody;
    }

    private HashMap<String, String[]> userRolesRequestBody() {
        HashMap<String, String[]> requestBody = new HashMap<>();
        String[] roles = new String[] {"ROLE_USER", "ROLE_SEARCH"};
        requestBody.put("roles", roles);

        return requestBody;
    }

    @Test(groups = "positive", priority = 1)
    public void testCreateUserSuccess() {
        Response response = given()
                .baseUri(baseUrl)
                .basePath("/api/users/create")
                .relaxedHTTPSValidation()
                .header("authorization", adminToken)
                .header("content-type", ContentType.JSON)
                .body(userParametersRequestBody())
                .when()
                .post();

        response.then().statusCode(200);
        userId = response.getBody().jsonPath().getJsonObject("id");
        System.out.println("Создан пользователь " + userId);
    }

    @Test(groups = "positive", priority = 2, dependsOnMethods = { "testCreateUserSuccess" })
    public void testSetUserRolesSuccess() {
        Response response = given()
                .baseUri(baseUrl)
                .basePath("/api/users/" + userId + "/roles")
                .relaxedHTTPSValidation()
                .header("authorization", adminToken)
                .header("content-type", ContentType.JSON)
                .body(userRolesRequestBody())
                .when()
                .patch();

        response.then().statusCode(200);
        ArrayList roles = new ArrayList(response.getBody().jsonPath().getJsonObject("roles"));
        System.out.println("Пользователю " + userId + " добавлены роли " + roles);
    }

    @Test(groups = "negative", priority = 3)
    public void testCreateUserUnauthorized() {
        Response response = given()
                .baseUri(baseUrl)
                .basePath("/api/users/create")
                .relaxedHTTPSValidation()
                .header("content-type", ContentType.JSON)
                .body(userParametersRequestBody())
                .when()
                .post();

        response.then().statusCode(401);
        System.out.println("Status code: " + response.statusCode());
    }

    @Test(groups = "negative", priority = 3)
    public void testCreateUserForbidden() {
        Response response = given()
                .baseUri(baseUrl)
                .basePath("/api/users/create")
                .relaxedHTTPSValidation()
                .header("authorization", userToken)
                .header("content-type", ContentType.JSON)
                .body(userParametersRequestBody())
                .when()
                .post();

        response.then().statusCode(403);
        System.out.println("Status code: " + response.statusCode());
    }

    @Test(groups = "negative", priority = 3)
    public void testSetUserRolesUnauthorized() {
        Response response = given()
                .baseUri(baseUrl)
                .basePath("/api/users/" + userId + "/roles")
                .relaxedHTTPSValidation()
                .header("content-type", ContentType.JSON)
                .body(userRolesRequestBody())
                .when()
                .patch();

        response.then().statusCode(401);
        System.out.println("Status code: " + response.statusCode());
    }

    @Test(groups = "negative", priority = 3)
    public void testSetUserRolesForbidden() {
        Response response = given()
                .baseUri(baseUrl)
                .basePath("/api/users/" + userId + "/roles")
                .relaxedHTTPSValidation()
                .header("authorization", userToken)
                .header("content-type", ContentType.JSON)
                .body(userRolesRequestBody())
                .when()
                .patch();
        response.then().statusCode(403);
        System.out.println("Status code: " + response.statusCode());
    }

    @Test(groups = "negative", priority = 3)
    public void testSetUserRolesNotFound() {
        Response response = given()
                .baseUri(baseUrl)
                .basePath("/api/users/12345600065544321/roles")
                .relaxedHTTPSValidation()
                .header("authorization", adminToken)
                .header("content-type", ContentType.JSON)
                .body(userRolesRequestBody())
                .when()
                .patch();
        response.then().statusCode(404);
        System.out.println("Status code: " + response.statusCode());
    }
}
