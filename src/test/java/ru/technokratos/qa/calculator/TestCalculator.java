package ru.technokratos.qa.calculator;

import org.testng.Assert;
import org.testng.annotations.Test;

import static ru.technokratos.qa.calculator.Calculator.*;

public class TestCalculator {

    @Test(groups = {"positive", "smoke"}, priority = 1)
    public void testIsCorrectOperationMultiplication() {
        boolean result = isCorrectOperation("*", 4);
        System.out.println(result);
        Assert.assertTrue(result);
    }

    @Test(groups = {"negative", "smoke"}, priority = 1)
    public void testIsCorrectOperationDivisionByZero() {
        boolean result = isCorrectOperation("/", 0);
        System.out.println(result);
        Assert.assertFalse(result);
    }

    @Test(groups = {"negative", "smoke"}, priority = 1, enabled = false)
    public void testIncorrectOperation() {
        boolean result = isCorrectOperation("//", 90);
        System.out.println(result);
        Assert.assertFalse(result);
    }

    @Test(groups = "positive", priority = 2)
    public void testCalculationMultiplication() {
        double result = calculationResult(8.2, "*", 2.1);
        Assert.assertEquals(result, 17.22);
        System.out.println(result);
    }

    @Test(groups = {"positive", "smoke"}, priority = 2)
    public void testCalculationDivision() {
        double result = calculationResult(6, "/", 3);
        Assert.assertEquals(result, 2);
        System.out.println(result);
    }

    @Test(expectedExceptions = {UnsupportedOperationException.class}, groups = {"negative", "smoke"}, priority = 2)
    public void testCalculationUnsupportedOperation() {
        double result = calculationResult(12, "vasya", 6.1);
        System.out.println(result);
    }

    @Test(groups = {"positive", "smoke"}, priority = 3)
    public void testResultSummation() {
        String result = operationResult(23, "+", 14.2);
        Assert.assertEquals(result, "Результат: 37.2");
    }

    @Test(groups = "positive", priority = 3)
    public void testResultSubtraction() {
        String result = operationResult(10.1, "-", 3.6);
        Assert.assertEquals(result, "Результат: 6.5");
    }

    @Test(groups = "positive", priority = 3)
    public void testResultMultiplyByZero() {
        String result = operationResult(15, "*", 0);
        Assert.assertEquals(result, "Результат: 0.0");
    }

    @Test(groups = {"negative", "smoke"}, priority = 3)
    public void testResultDivideByZero() {
        String result = operationResult(1, "/", 0);
        Assert.assertEquals(result, "Ошибка");
    }

    @Test(groups = "negative", priority = 3)
    public void testResultIncorrectOperation() {
        String result = operationResult(3.5, "12", 3);
        Assert.assertEquals(result, "Ошибка");
    }
}
